<?php


namespace App\Http\Controllers;

use App\Models\Employees;
use App\Models\Images;
use Illuminate\Http\Request;
use App\Http\Resources\EmployeesResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use DB;

class EmployeesController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $employeesQuery = EmployeesResource::collection(Employees::all());
        return [
            'Employees' => $employeesQuery
        ];

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $params = $request->all();
        $employees = Employees::create([
            'name' => $params['name'],
            'email' => $params['email'],
            'phone' => $params['phone'],
            'location' => $params['location'],
                 ]);
        new EmployeesResource($employees);
        return response()->json("Employee has been added Successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employees  $Employees
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        // $employees=Employees::findorfail($id)->image;

        $employee = DB::table('employees')
        ->select('*')
        ->join('images','images.employee_id','=','employees.id')
        ->where('employees.id',$id)
        ->first();

        return [
            'Employees' => $employee
        ];

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employees  $Employees
     * @return \Illuminate\Http\Response
     */
    public function edit(Employees $Employees)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employees  $Employees
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if ($id === null) {
            return response()->json(['error' => 'Employees not found'], 404);
        }else{
            $employees=Employees::findorfail($id);
                $params = $request->all();
              $employees->update($params);
          new EmployeesResource($employees);

          return response()->json("Employee has been updated Successfully");

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employees  $Employees
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employees=Employees::find($id);

        if ($employees->delete()) {
            return response()->json('Employee deleted Succefully');
        } else {
            abort(404,'Employee not found.');
        }
    }

    public function image($id)
    {
        //
        $image = Images::where('employee_id',$id)->get();

        return [
            'images' => $image
        ];
    }

    public function uploadImage(Request $request){

        $params = $request->all();

        $image=$request->file('image');
        //  return  $image;
        if($image){
            $imageName=$image->getClientOriginalName();
            $destination = base_path() . '/public/images';
            $image->move($destination,$imageName);

            $formInput['image']=$imageName;
            $formInput['employee_id']=$request->employee_id;

            Images::create($formInput);


        return response()->json("Image has been added Successfully");

    }
}

}
