<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Images;

class Employees extends Model
{

    use HasFactory;
    protected $fillable = ['name','phone','email','location'];


    public function image()
    {
        return $this->hasOne(Images::class, 'foreign_key', 'employee_id');
    }
}

