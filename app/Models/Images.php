<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Employees;

class Images extends Model
{
    use HasFactory;
    protected $fillable = ['image','employee_id'];

    public function employee()
    {
        return $this->belongsTo(Employees::class);
    }
}
